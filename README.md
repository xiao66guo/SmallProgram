# SmallProgram
基于微信开发者工具开发的一款简易的小程序
```
用微信开发工具 v1.0.2.1904090 开发的一个简易的小程序，无论是iPhone、Android还是平板都可以展示的一个小程序

项目中的每个模块都有四个文件：***.js、***.json、***.wxml、***wxss

项目中的配置文件以及界面的的展示样式都在 app.json 和 app.wxss 文件中实现，
```
```Android端展示```
![输入图片说明](https://images.gitee.com/uploads/images/2019/0511/154012_b4daae82_638207.png "Android.png")

 
```iOS端展示```
![输入图片说明](https://images.gitee.com/uploads/images/2019/0511/154041_8d9db224_638207.png "iPhone.png")

```pad端展示```
![输入图片说明](https://images.gitee.com/uploads/images/2019/0511/154107_9c9acc04_638207.png "平板.png")
